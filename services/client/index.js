import http from 'http';
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import routes from './routes';
import passport from 'passport';
import FacebookToken from './config/FacebookToken';
import FacebookTokenStrategy from 'passport-facebook-token';
import mongoose from 'mongoose';
import methodOverride from 'method-override';
let app = express();
app.server = http.createServer(app);

app.use(morgan('dev'));

let mongoDB = 'mongodb://mongodb/chatbot';
mongoose.connect(mongoDB, {
  useMongoClient: true
});
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());
app.use(methodOverride());

passport.serializeUser((user, done)  => done(null, user));
  
passport.deserializeUser((user, done)=> done(null, user));

passport.use(FacebookToken.getStrategyToken())

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use('/client',routes);











app.server.listen(process.env.PORT || 3000, () => {
    console.log(`Started on port ${app.server.address().port}`);
});

