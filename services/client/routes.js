import express from 'express';
import passport from 'passport';

const router = express.Router();

import Login from './src/api/login';



router.get('/health-check', (req, res) =>
res.send('OK')
);


router.post('/auth/facebook/token',passport.authenticate('facebook-token'),Login.facebook);

router.post('/getSession',Login.getSession);


export default router;


