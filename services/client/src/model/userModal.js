import mongoose from 'mongoose';
import findOrCreate from 'mongoose-findorcreate';

const UserSchema = mongoose.Schema({
    facebookId: {
        type: String,
    },
}, {collection : 'user'});

UserSchema.plugin(findOrCreate);


let userModal = mongoose.model('user', UserSchema);

export default userModal;