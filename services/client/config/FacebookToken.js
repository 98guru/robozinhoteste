import FacebookTokenStrategy from 'passport-facebook-token';
import Q from 'q';
import userModal from '../src/model/userModal';

export default class FacebookToken {
    static getStrategyToken(){ 
        return new FacebookTokenStrategy({
            clientID: '1940829806241241',
            clientSecret: 'e64e0c5c545734afe8f5af46f269d9cc'
          }, (accessToken, refreshToken, profile, done) => {
              
              userModal.findOrCreate({profile:profile},  (error, user) => {
                return  done(error, user)
            });
          }
        )
    }
    
}
