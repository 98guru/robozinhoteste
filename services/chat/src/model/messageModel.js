import mongoose from 'mongoose';

const MessageSchema = mongoose.Schema({
    channel: {
        type: String,
        required: true,
        unique: true, 
        index: true
    },
    user: {
        type: String,
        required: true,
        unique: true, 
        index: true
    },
    messages: {
        type: Array,
        required: false,
    }
}, {collection : 'message'});

let messageModel = mongoose.model('Message', MessageSchema);

export default messageModel;