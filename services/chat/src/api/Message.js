
import messageModel from '../model/messageModel';
import Q from 'q';
import Telegraf from 'telegraf';

const botKey = '391588725:AAFits1UyeyRF0Wxt5vdl9tQ00oOejjPiq8';
const bot = new Telegraf(botKey);

export default class Message{

    receive({data,socket}){
        this.saveMessage({data,socket})
        .then(() =>this.renponseTelegram(data))
    }

    renponseTelegram(data){
        bot.telegram.sendMessage(data.channel, data.body)
    }
    response(socket,data){
        socket.join(data.channel.toString());
        socket.broadcast.emit(data.channel.toString(),{body:data.body,type:data.type});
    }
    saveMessage({data,socket}){
        let deferred = Q.defer();
        messageModel.findOne({channel:data.channel},(err,channel) =>{  
            if(!channel){
                this.saveNewChannel(data).then((channelSaved)=> deferred.resolve())
                return;
            }
            channel.messages.push(data);
            channel.save((err, channelUpdated)  =>  deferred.resolve());
        })
        return deferred.promise;
    }

    saveNewChannel(data){
        let deferred = Q.defer();
        messageModel.create({channel:data.channel,user:data.user,messages:[data]},(err,channel) =>{
            console.log(err)
            if(!err) deferred.resolve(channel);
            deferred.reject(err);

        })
        return deferred.promise;
    }

    fetchChannels({socket}){
    
        messageModel.find({},(err,result) =>{
            let channels =[] 
            if(result.length >0){
                channels =  result.map((item) =>(
                    { 
                        name:item.channel,
                        messages:item.messages,
                        user:item.user
                    }
                ))
            }
            socket.emit('fetchChannels',channels);
            
        })
    }

    onTelegramMessage({socket}){
        bot.on('message',(ctx) =>{
            const {from,date,text} = ctx.update.message;
            let data ={
                channel:from.id,
                type:(from.is_bot?'bot':'user'),
                date,
                body:text,
                messages:[],
                user:`${from.first_name} ${from.last_name}`
            }
            this.saveMessage({data,socket})
            .then(() =>{
                this.response(socket,data)
            })
        })
        bot.startPolling()
    }
    

}