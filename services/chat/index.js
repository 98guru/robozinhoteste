import http from 'http';
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import SocketIO from 'socket.io';
import methodOverride from 'method-override';
import Message from './src/api/Message';
import mongoose from 'mongoose';
let app = express();
app.server = http.createServer(app);
let io = new SocketIO(app.server,{path: '/'});

let message = new Message();

io.on('connection', (socket) => {
     socket.on('message',data => message.receive({data,socket}))
     socket.on('fetchChannels',data => message.fetchChannels({socket}))
     message.onTelegramMessage({socket})
})

let mongoDB = 'mongodb://mongodb/chatbot';
mongoose.connect(mongoDB, {
  useMongoClient: true
});
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(morgan('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(methodOverride());




app.server.listen(process.env.PORT || 3000, () => {
    console.log(`Started on port ${app.server.address().port}`);
});

