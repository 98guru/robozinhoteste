import React from 'react';
import {connect} from 'react-redux';
import styles from './header.css'
import * as actions from '../user/login.actions';



const Header  = props => {
  const {history} = props;

  return(<div className="header col-sm-12">
        <div className="btn-exit" onClick={()=>{ props.logout(history)}}>
          Logout
        </div>
  </div>)
}


const mapStateToProps = state => (
  {
      
  }
);


export default connect(mapStateToProps, {...actions})(Header);