import {combineReducers} from 'redux';
import chat from './chat/chat.reducers';
import login from './user/login.reducers'


const rootReducer = combineReducers({
    login,
    chat
  });
  
  export default rootReducer;