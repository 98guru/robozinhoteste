// src/routes.js
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom'

import App from './App';
import {
  createStore,
  applyMiddleware 
} from 'redux';
import ReduxThunk from 'redux-thunk';
import IndexReducer from './reducers';
import { Provider } from 'react-redux';
import {Chat} from './chat';
import {Login} from './user';


let store = createStore(IndexReducer,applyMiddleware(ReduxThunk))
 
const Routes = (props) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route path="/chat" component={Chat} />
        <Route path="/login" component={Login} />
      </Switch>
    </Router>
  </Provider>
); 
 
export default Routes;