import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {connect} from 'react-redux';
import *  as actions from './user/login.actions';


class App extends Component {

  componentDidMount(){
    const { history } = this.props;
    this.props.verifySession(history);
  }
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => (
  {
     
  }
);

export default connect(mapStateToProps, {...actions})(App);
