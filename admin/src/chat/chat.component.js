import React from 'react';
import {connect} from 'react-redux';
import * as actions from './chat.actions';
import PropTypes from 'prop-types';

import io from 'socket.io-client';
import style from './chat.css';
import MessageItem from './message-item.component';
import {Header} from '../layout';
import Channels from './channels.component';


const socket = io('localhost:3000', { path: '/chat' });

class Chat extends React.Component {


  componentDidMount() {
     let socket = io('localhost:3000', { path: '/chat' });
     this.props.defineSocket({socket:socket});
     this.props.getChannels({socket:socket});
  }

  
  renderMessages(){
      const {messages} = this.props;
      let itens = []
      if(messages.length >0){
        messages.map((item,key) => itens.push(<MessageItem key={key} data={item}/>))
      }
      console.log(messages);
      
    return (itens.length>0)?itens:(<div>NOT CHATS YET!</div>);
  }

  onSend(){
    const {message,socket,channelSelected} = this.props;
    this.props.addMessage({message,socket,channelSelected,type:'bot'});
  }

  renderMessageContainer(){
    const {chatOpened} = this.props;
     let container = (<div className="chat-text">
                         <div className="chat-text-inner">
                            <textarea value={this.props.message} onChange={(event) => this.props.setMessage(event.target.value)} rows="4" cols="50"></textarea>
                            <button onClick={this.onSend.bind(this)}>Send</button>
                         </div>
                    </div>)
    return(chatOpened)?container:(<div></div>);
  }
  

  render() {
    const {history} = this.props;
    return(
      <div>
        <Header history={history}/>
         <div className="App">
            <div className="channels col-sm-2 col-md-2">
                  <Channels/>
            </div>
            <div className="chat col-sm-10 col-md-10">
                    <div className="panel-chat">
                    {this.renderMessages()}
                    </div>
                   {this.renderMessageContainer()} 
                   
            </div>
           
        </div>
      </div>
    );
  }
}


Chat.propTypes = {
    messages: PropTypes.array.isRequired,
};





const mapStateToProps = state => (
    {
        messages:state.chat.channelSelected.messages,
        message:state.chat.message,
        socket:state.chat.socket,
        chatOpened:state.chat.chatOpened,
        channelSelected:state.chat.channelSelected
    }
);


export default connect(mapStateToProps, {...actions})(Chat);