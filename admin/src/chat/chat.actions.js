import * as types from '../constants/types';
import * as moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-BR');


export const addMessage = ({message,socket,channelSelected,type,emit = true}) => {
    let messageObj = {
        body:message,
        date:Date.now(),
        channel:channelSelected.name,
        type:type
    }
    if(emit)socket.emit('message', messageObj);
    return{
        type: types.ADD_MESSAGE,
        message:messageObj
    }
}




export const setMessage = (message) => (
    {
         type: types.CHANGE_MESSAGE_INPUT,
        message
    }
)


export const getChannels = ({socket}) => dispatch =>{
    socket.emit('fetchChannels');
    socket.on('fetchChannels',(channels) =>{
        dispatch({
            type:types.GET_CHANNELS,
            channels
        })

    });
    
}

const receiveMessage = ({channel,data,dispatch,socket}) => {
    dispatch(addMessage({message:data.body,socket,channelSelected:channel,type:'user',emit:false}))
}


export const openChat = ({prevChannelSelected,channel,socket}) =>dispatch => {
    if(prevChannelSelected.name)socket.removeListener(prevChannelSelected.name);
    socket.removeListener(channel.name);
    console.log(channel.name.toString());
    socket.on(channel.name.toString(),(data) => {
        console.log(data);
        receiveMessage({channel,data,dispatch,socket})
    })
    dispatch({
        type:types.OPEN_CHAT,
        channelSelected:channel,
    })
}

export const defineSocket = ({socket}) =>(
    {
        type:types.DEFINE_SOCKET,
        socket:socket
    }
)