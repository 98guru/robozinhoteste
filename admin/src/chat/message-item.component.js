import React from 'react';
import {connect} from 'react-redux';
import * as actions from './chat.actions';
import PropTypes from 'prop-types';
import * as moment from 'moment';
import io from 'socket.io-client';
import style from './message-item.css';
import 'moment/locale/pt-br';
moment.locale('pt-BR');


const MessageItem  = props => (
  <div className="messageItem">
    <div className={`messageItemOuter ${props.data.type}`}>
      <div className="body">{props.data.body}</div>
      <div className="date">{moment(props.data.date).format('DD/MM/YY HH:mm:ss')}</div>
    </div>
  </div>
)

export default MessageItem;