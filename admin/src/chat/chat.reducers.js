import * as types from '../constants/types';

const initialState = {
  loaded: false,
  messages:[],
  message:'',
  channels:[],
  chatOpened:false,
  channelSelected:{
    messages:[]
  },
  socket:{}
};

const chat = (state = initialState, action) => {
  switch (action.type) {
  case types.ADD_MESSAGE:
     console.log(action);
    let channelSelected = {
      ...state.channelSelected,
      messages:[
        ...state.channelSelected.messages,
        action.message
      ]
    }
    let channelsChanged = state.channels.map(item =>{ 
        if(item == state.channelSelected) item = channelSelected;
        return item;
    })
    return {
       ...state,
       channelSelected:channelSelected,
       channels:channelsChanged,
       message:''
    };
  case types.RECEIVE_MESSAGE:
    return {
      ...state,
      data: action.data
    };
  case types.CHANGE_MESSAGE_INPUT:
    return {
      ...state,
      message:action.message
    }
  case types.GET_CHANNELS:
    return{
      ...state,
      channels:action.channels
    }
  case types.OPEN_CHAT:
    return{
      ...state,
      channelSelected:action.channelSelected,
      chatOpened:true
      
    }
  case types.DEFINE_SOCKET:
    return{
      ...state,
      socket:action.socket    
    }
  default:
    return state;
  }
}

export default chat;