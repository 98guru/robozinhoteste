import React from 'react';
import {connect} from 'react-redux';
import * as actions from './chat.actions';
import PropTypes from 'prop-types';

import style from './channels.css';



class Channel extends React.Component {

  click(data){
    const{socket,prevChannelSelected} = this.props;
    this.props.openChat({prevChannelSelected,channel:data,socket})

  }

  render(){
    let {data} = this.props;
    return(
      <div className="channelItem" onClick={()=>this.click(data)}>
            <div className="user">{data.user}</div>
            <div className="date">{data.lastTime}</div>
      </div>

    )
  }

}



const mapStateToProps = state => (
  {
    socket:state.chat.socket,
    prevChannelSelected: state.chat.channelSelected
  }
);


export default connect(mapStateToProps, {...actions})(Channel);