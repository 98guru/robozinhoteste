import React from 'react';
import {connect} from 'react-redux';
import * as actions from './chat.actions';
import PropTypes from 'prop-types';

import style from './chat.css';
import Channel from './channel.component';

class Channels extends React.Component {



    componentWillReceiveProps(nextProps){

    }


    renderChannels(){
        const {channels} = this.props;
        let itens = []
        if(channels.length >0){
          channels.map((item,key) => itens.push(<Channel key={key} data={item}/>))
        }
      console.log(channels)
      return (itens.length)?itens:(<div></div>);
    }
  

  render() {
    return(
      <div className="chanelContainer">
          {this.renderChannels()}
      </div>
    );
  }
}




const mapStateToProps = state => (
    {
        channels:state.chat.channels, 
        socket:state.chat.socket 
    }
);


export default connect(mapStateToProps, {...actions})(Channels);