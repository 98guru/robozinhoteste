import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import styles from './login.css';
import {Glyphicon} from 'react-bootstrap';
import AccountKit from 'react-facebook-account-kit';
import FacebookLogin from 'react-facebook-login';
import *  as actions from './login.actions';
let FontAwesome = require('react-fontawesome');
class Login extends React.Component {



  responseFacebook = (response) => {
    const { history } = this.props;
    this.props.saveSession(response,history)
  }

  render() {
    return(
      <div>
         <div className="login-container">
            <div className="login-inner">
                <h3 className="login-title">
                    Bot Admin
                </h3>
                <FacebookLogin
                    appId="1940829806241241"
                    autoLoad={false}
                    fields="name,email,picture"
                    callback={this.responseFacebook}
                    cssClass="my-facebook-button-class"
                    icon="fa-facebook"
                    textButton="Access via Facebook"
                />
            </div>
        </div>
      </div>
    );
  }
}



const mapStateToProps = state => (
    {
        user: state.login.user
    }
);


export default connect(mapStateToProps, {...actions})(Login);