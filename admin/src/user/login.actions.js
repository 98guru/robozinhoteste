import * as types from '../constants/types';


import axios from 'axios';




export const saveSession = (response,history) => dispatch =>{
    let accessToken = response.accessToken;
    sessionStorage.setItem('accessToken',accessToken);
    session(accessToken).then(()=>{
        history.push('/chat');
    })
    
}

export const verifySession = (history) => dispatch =>{
    let accessToken = sessionStorage.getItem('accessToken');
    console.log(accessToken);
    session(accessToken).then(()=>{
        history.push('/chat');
    }).catch(()=>history.push('/login'))
}

const session = (accessToken) => {
    return axios.post('http://localhost:3000/client/auth/facebook/token',{
        access_token:accessToken
    })
}

export const logout = (history) => dispatch =>{
    console.log(history)
    sessionStorage.removeItem('accessToken');
    history.push('/')

}